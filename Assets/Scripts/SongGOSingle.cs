﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SongGOSingle : MonoBehaviour
{
    public static SongGOSingle instance;

    [SerializeField] float unpausedVolume;
    [SerializeField] float pausedVolume;

    [SerializeField] AudioSource song_audiosrc;
    void Awake()
    {
        if (SongGOSingle.instance == null)
        {
            SongGOSingle.instance = this;
        }
        else if (SongGOSingle.instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        RestartSong();
    }

    public void PlayMode(string mode)
    {
        if(mode == "paused")
        {
            song_audiosrc.volume = pausedVolume;
        }
        else if (mode == "unpaused")
        {
            song_audiosrc.volume = unpausedVolume;
        }
    }

    public bool IsPlaying()
    {
        return song_audiosrc.isPlaying;
    }

    public void RestartSong()
    {
        if (!song_audiosrc.isPlaying)
        {
            song_audiosrc.Play();
        }
        else
        {
            song_audiosrc.Stop();
            song_audiosrc.Play();
        }
    }

    public void StopSong()
    {
        song_audiosrc.Stop();
    }
}
