﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Snowball : MonoBehaviour
{
    public enum State
    {
        SMALL,
        TREEDROPPER,
        TOWNDROPPER
    }

    public UnityEvent onCanDropTrees;
    public UnityEvent onCanDropTowns;

    private Rigidbody2D rb;

    [SerializeField] float start_scale;
    [SerializeField] float grow_rate;
    [SerializeField] float jump_force;
    [SerializeField] float skiier_growth;
    public float treeDropperScaleAmount;
    public float townDropperScaleAmount;

    private bool _grounded = false;
    private bool _jump = false;
    private Vector2 _ground_normal = Vector2.up;
    private Vector2 _jump_dir = new Vector2(1f, 3f).normalized;

    private State _state;
    public State state
    {
        get { return _state; }
        set
        {
            if (_state == State.SMALL && value == State.TREEDROPPER)
            {
                // fire some event
                onCanDropTrees.Invoke();
                _state = value;
            }
            else if (_state == State.TREEDROPPER && value == State.TOWNDROPPER)
            {
                // fire endgame event
                onCanDropTowns.Invoke();
                _state = value;
            }
        }
    }

    [SerializeField] AudioSource roll_audiosrc;
    [SerializeField] AudioSource jump_audiosrc;
    [SerializeField] AudioSource land_audiosrc;
    [SerializeField] AudioSource skier_audiosrc;
    [SerializeField] AudioSource treeHit_audiosrc;

    bool canPlayTreeHitSound = true;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        roll_audiosrc = GetComponent<AudioSource>();
    }

    void Start()
    {
        _state = State.SMALL;
        transform.localScale = new Vector3(1f, 1f) * start_scale; // 0.1f * transform.localScale;

        roll_audiosrc.Play();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (_grounded && Time.timeScale == 1)
            {
                _jump = true;
                jump_audiosrc.Play();
            }
        }

        if (transform.localScale.x >= townDropperScaleAmount)
        {
            state = State.TOWNDROPPER;
        }

        if (transform.localScale.x >= treeDropperScaleAmount)
        {
            state = State.TREEDROPPER;
        }
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        if (_grounded)
        {
            if (_jump)
            {
                _jump = false;
                rb.AddForce(_jump_dir * jump_force * rb.mass, ForceMode2D.Impulse);
            }
            //print("ang vel: " + rb.angularVelocity);
            if (Mathf.Abs(rb.angularVelocity) > 100f)
            {
                //transform.localScale += new Vector3(1, 1) * grow_rate * Time.fixedDeltaTime;
                IncreaseSize(grow_rate * Time.fixedDeltaTime);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("skiier"))
        {
            skier_audiosrc.Play();
            Destroy(collision.gameObject);
            //transform.localScale += new Vector3(1, 1) * skiier_growth;
            IncreaseSize(skiier_growth);
        }

        if (collision.gameObject.CompareTag("tree"))
        {
            print("Entering tree");
            if (canPlayTreeHitSound)
            {
                treeHit_audiosrc.Play();
                canPlayTreeHitSound = false;
            }

            if (transform.localScale.x >= 1.5f)
            {
                var tree_rb = collision.gameObject.GetComponent<Rigidbody2D>();
                tree_rb.gameObject.GetComponent<Collider2D>().enabled = false;
                tree_rb.bodyType = RigidbodyType2D.Dynamic;
                tree_rb.gameObject.layer = LayerMask.NameToLayer("YGroundYSnowball");
                tree_rb.gameObject.GetComponent<Tree>().fallen = true;
                //collision.transform.SetParent(transform);
            }
        }

        if (collision.gameObject.CompareTag("SnowGround"))
        {
            print(collision.relativeVelocity.y);
            //collision.relativeVelocity.y
            land_audiosrc.volume = collision.relativeVelocity.y * 0.1f;
            land_audiosrc.Play();
            roll_audiosrc.UnPause();
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("SnowGround"))
        {
            //print("On gound");
            _grounded = true;
            _ground_normal = collision.contacts[0].normal.normalized;
        }

        if (collision.gameObject.CompareTag("tree"))
        {
            print("Staying tree");
            canPlayTreeHitSound = false;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("SnowGround"))
        {
            //print("Off gound");
            _grounded = false;
            roll_audiosrc.Pause();
        }
        
        if (collision.gameObject.CompareTag("tree"))
        {
            print("Exiting tree");
            canPlayTreeHitSound = true;
        }
    }

    void IncreaseSize(float amount)
    {
        float oldScale = transform.localScale.x;
        float newScale = transform.localScale.x + amount;

        transform.localScale = new Vector3(1, 1) * newScale;

        //if (oldScale < treeDropperScaleAmount && newScale > treeDropperScaleAmount )
        //{
        //    state = State.TREEDROPPER;
        //} else if (oldScale < townDropperScaleAmount && newScale > townDropperScaleAmount)
        //{
        //    state = State.TOWNDROPPER;
        //}
    }
}
