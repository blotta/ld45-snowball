﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPauseButton : MonoBehaviour
{
    [SerializeField] GameObject pauseQuitButton;

    public void ToggleQuitButton()
    {
        if (pauseQuitButton.activeSelf == true)
        {
            HideQuitButton();
        }
        else
        {
            ShowQuitButton();
        }
    }

    public void ShowQuitButton()
    {
        pauseQuitButton.SetActive(true);
    }

    public void HideQuitButton()
    {
        pauseQuitButton.SetActive(false);
    }
}
