﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    //[SerializeField] GameObject snowball;
    //[SerializeField] GameObject ground;
    [SerializeField] GameObject finishPanel;

    void Awake()
    {
        if (GameManager.instance == null)
        {
            GameManager.instance = this;
        }
        else if (GameManager.instance != this)
        {
            Destroy(gameObject);
        }

        Screen.fullScreen = false;
        Screen.fullScreenMode = FullScreenMode.Windowed;

        //DontDestroyOnLoad(gameObject);
    }

    public void ShowFinishPanel()
    {
        finishPanel.SetActive(true);
    }

    public void TogglePause()
    {
        if (Time.timeScale < 1)
        {
            UnpauseGame();
        }
        else
        {
            PauseGame();
        }
    }

    public void PauseGame()
    {
        Time.timeScale = 0f;
        SongGOSingle.instance.PlayMode("paused");
    }

    public void UnpauseGame()
    {
        Time.timeScale = 1f;
        SongGOSingle.instance.PlayMode("unpaused");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Game");
    }
}
