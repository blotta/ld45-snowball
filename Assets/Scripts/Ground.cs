﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class Ground : MonoBehaviour
{
    public enum SpawnMode
    {
        NORMAL,
        ENDGAME
    }

    public SpawnMode spawnMode;

    GameObject snowball;

    public SpriteShapeController sprShapeCtrl;
    private Spline spline;

    public GameObject treePrefab;
    public GameObject skiierPrefab;
    public GameObject townPrefab;

    private Vector3 lastPoint;
    public float min_width = 2f;
    public float max_width = 3f;
    public float min_height = 0.5f;
    public float max_height = 2f;
    public int gen_num = 10;

    bool townSpawned = false;

    void Awake()
    {
        spawnMode = SpawnMode.NORMAL;

        spline = sprShapeCtrl.spline;
        //for (int i = 0; i < spline.GetPointCount(); i++)
        //{
        //    print(spline.GetPosition(i));
        //}
    }

    private void Start()
    {
        lastPoint = spline.GetPosition(spline.GetPointCount() - 1);
        snowball = GameObject.FindGameObjectWithTag("Snowball");
    }

    void Update()
    {
        //print(spawnMode.ToString());
        if (spawnMode == SpawnMode.NORMAL)
        {
            CheckAndAddSection();
        }
        else if (spawnMode == SpawnMode.ENDGAME)
        {
            if (!townSpawned)
            {
                AddEndSection();
            }
        }
    }

    void CheckAndAddSection()
    {
        //print("CheckAndAddSection");
        Vector2 bottomRightScreen = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0f));
        //if (bottomRightScreen.x + 15f > lastPoint.x || bottomRightScreen.y - 10f < lastPoint.y)
        if (Vector3.Distance(snowball.transform.position, lastPoint) < 40f)
        {
            //print("Adding to ground");
            var addTree = false;
            if (Random.Range(0, 10) >= 5) { addTree = true; }

            var addSkiier = false;
            if (Random.Range(0, 10) >= 5) { addSkiier = true; }

            for (int i = 0; i < gen_num; i++)
            {
                Vector3 newOffset = new Vector3(Random.Range(min_width, max_width), -1 * Random.Range(min_height, max_height));
                var newPoint = AddPoint(newOffset);

                if (addTree && i == 5)
                {
                    Instantiate(treePrefab, newPoint + (new Vector3(2, 3)).normalized * 3.66f, Quaternion.identity);
                }

                if (addSkiier && i == 1)
                {
                    Instantiate(skiierPrefab, newPoint + (new Vector3(2, 3)).normalized * 4f, Quaternion.identity);
                }
            }
        }
    }

    void AddEndSection()
    {
        var townPoint = AddPoint(Vector3.right * 5f);
        AddPoint(Vector3.right * 5f);
        AddPoint(new Vector3(25f, 30f));
        Instantiate(townPrefab, townPoint + Vector3.up * 3.8f, Quaternion.identity);
        townSpawned = true;
    }

    Vector3 AddPoint(Vector3 offset)
    {
        //var lastPoint = spline.GetPosition(spline.GetPointCount() - 1);
        var newPoint = lastPoint + offset;
        spline.InsertPointAt(spline.GetPointCount(), newPoint);
        spline.SetTangentMode(spline.GetPointCount() - 1, ShapeTangentMode.Continuous);
        // spline.SetCorner(spline.GetPointCount() - 1, true);
        lastPoint = spline.GetPosition(spline.GetPointCount() - 1);
        return newPoint;
    }

    public void ChangeToEndGame()
    {
        spawnMode = SpawnMode.ENDGAME;
    }
}
