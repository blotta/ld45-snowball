﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Town : MonoBehaviour
{
    bool dropped = false;
    IEnumerator drop_c = null;

    [SerializeField] AudioSource chaos_audiosrc;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Snowball"))
        {
            if (!dropped)
            {
                drop_c = DropTownCoroutine();
                StartCoroutine(drop_c);
                dropped = true;
            }
        }
    }

    IEnumerator DropTownCoroutine()
    {
        GameManager.instance.ShowFinishPanel();

        Vector3 vertPos;
        float downSpeed = 1f;
        float vShakeRange = 0.9f;
        float hShakeRange = 2f;

        float alpha_rate = 0.1f;
        SpriteRenderer sr = GetComponent<SpriteRenderer>();

        chaos_audiosrc.Play();

        while (true)
        {
            vertPos = transform.position + Vector3.down * downSpeed * Time.deltaTime;

            Vector3 shakePos = vertPos +
                new Vector3(Random.Range(-hShakeRange, +hShakeRange), Random.Range(-vShakeRange, +vShakeRange)) * Time.deltaTime;

            transform.position = shakePos;

            var curr_color = sr.color;
            curr_color.a -= alpha_rate * Time.deltaTime;
            sr.color = curr_color;

            if (curr_color.a == 0)
                break;

            yield return null;
        }

        Destroy(gameObject);
    }
}
