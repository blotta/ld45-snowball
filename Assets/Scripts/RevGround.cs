﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

struct MostPointsIdx
{
    int left;
    int top;
    int right;
    int bottom;
}

public class RevGround : MonoBehaviour
{
    public SpriteShapeController sprShapeCtrl;
    private Spline spline;

    //private int rightMostPoint {  get { return RightMostPoint(); } }
    //private int nextGroundPoint { get { return BottomMostPoint(); } }
    //private int nextPivotPoint { get { return LeftMostPoint(); } }
    //private int topMostPoint {  get { return TopMostPoint(); } }

    public float min_width = 2f;
    public float max_width = 3f;
    public float min_height = 0.5f;
    public float max_height = 2f;

    private void Awake()
    {
        spline = sprShapeCtrl.spline;
    }

    void Start()
    {
        print(MostPoints());

        
    }

    void Update()
    {
        Vector2 bottomRightScreen = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0f));
        Vector4 mostPoints = MostPoints();
        Vector3 rightMostPoint = spline.GetPosition((int)mostPoints.z);
        if (bottomRightScreen.x > rightMostPoint.x || bottomRightScreen.y < rightMostPoint.y )
        {
            Vector3 newOffset = new Vector3(Random.Range(min_width, max_width), -1 * Random.Range(min_height, max_height));
            //var newPoint = MovePoint((int)mostPoints.w, (int)mostPoints.z, newOffset);
            AddToGround(mostPoints, newOffset);
        }
        
    }

    void AddToGround(Vector4 mostPointsIdxs, Vector3 rightOffset)
    {
        float defaultOffset = 10f;

        int leftIdx = (int)mostPointsIdxs.x;
        int topIdx = (int)mostPointsIdxs.y;
        int rightIdx = (int)mostPointsIdxs.z;
        int botIdx = (int)mostPointsIdxs.w;

        if (leftIdx != topIdx && leftIdx != rightIdx && leftIdx != botIdx && topIdx != rightIdx && topIdx != botIdx && rightIdx != botIdx)
        {
            print("Points OK");
        }
        else
        {
            print("Points not OK!!");
            return;
        }

        Vector3 oldLeftPoint = spline.GetPosition(leftIdx);
        Vector3 oldTopPoint = spline.GetPosition(topIdx);
        Vector3 oldRightPoint = spline.GetPosition(rightIdx);
        Vector3 oldBotPoint = spline.GetPosition(botIdx);

        // move bot point to right
        Vector3 newRightPoint = oldRightPoint + rightOffset;// MovePoint(botIdx, rightIdx, rightOffset);
        spline.SetPosition(botIdx, newRightPoint);
        int newRightIdx = botIdx;
        spline.SetTangentMode(newRightIdx, ShapeTangentMode.Linear);
        //spline.SetLeftTangent(newRightIdx, (oldTopPoint - newRightPoint).normalized);
        //spline.SetRightTangent(newRightIdx, rightOffset.normalized);

        // move left point to bot
        Vector3 newBotPoint = new Vector3(oldBotPoint.x + defaultOffset, newRightPoint.y - defaultOffset);
        spline.SetPosition(leftIdx, newBotPoint);
        int newBotIdx = leftIdx;
        spline.SetTangentMode(newBotIdx, ShapeTangentMode.Linear);
        //spline.SetLeftTangent(newBotIdx, (newRightPoint - newBotPoint).normalized);
        //spline.SetRightTangent(newBotIdx, Vector3.left);

        // move top point to left
        Vector3 newLeftPoint = new Vector3(oldTopPoint.x - defaultOffset, newRightPoint.y);
        spline.SetPosition(topIdx, newLeftPoint);
        int newLeftIdx = topIdx;
        spline.SetTangentMode(newLeftIdx, ShapeTangentMode.Linear);
        //spline.SetLeftTangent(newLeftIdx, Vector3.down);
        //spline.SetRightTangent(newLeftIdx, Vector3.up);
    }

    //Vector3 MovePoint(int pointIdxToMove, int refPointIdx, Vector3 offset)
    //{
    //    Vector3 newPointPos = spline.GetPosition(refPointIdx) + offset;
    //    spline.SetPosition(pointIdxToMove, newPointPos);
    //    return newPointPos;
    //}

    Vector4 MostPoints()
    {
        // left, top, right, bottom
        Vector4 idxs = new Vector4(0, 0, 0, 0);
        Vector4 mostVals = new Vector4(0f, 0f, 0f, 0f);

        for (int i = 0; i < spline.GetPointCount(); i++)
        {
            Vector3 thisPoint = spline.GetPosition(i);
            // left
            if (thisPoint.x < mostVals.x)
            {
                idxs.x = i;
                mostVals.x = thisPoint.x;
            }

            // top
            if (thisPoint.y > mostVals.y)
            {
                idxs.y = i;
                mostVals.y = thisPoint.y;
            }

            // right
            if (thisPoint.x > mostVals.z)
            {
                idxs.z = i;
                mostVals.z = thisPoint.x;
            }

            // bottom
            if (thisPoint.y < mostVals.w)
            {
                idxs.w = i;
                mostVals.w = thisPoint.y;
            }
        }
        // print(mostVals);
        return idxs;
    }
    int RightMostPoint()
    {
        int idx = 0;
        float mostVal = 0f;
        for (int i = 0; i < spline.GetPointCount(); i++)
        {
            float thisVal = spline.GetPosition(i).x;
            if (thisVal > mostVal)
            {
                idx = i;
                mostVal = thisVal; 
            }
        }
        return idx;
    }

    int BottomMostPoint()
    {
        int idx = 0;
        float mostVal = 0f;
        for (int i = 0; i < spline.GetPointCount(); i++)
        {
            float thisVal = spline.GetPosition(i).y;
            if (thisVal < mostVal)
            {
                idx = i;
                mostVal = thisVal; 
            }
        }
        return idx;
    }

    int LeftMostPoint()
    {
        int idx = 0;
        float mostVal = 0f;
        for (int i = 0; i < spline.GetPointCount(); i++)
        {
            float thisVal = spline.GetPosition(i).x;
            if (thisVal < mostVal)
            {
                idx = i;
                mostVal = thisVal; 
            }
        }
        return idx;
    }

    int TopMostPoint()
    {
        int idx = 0;
        float mostVal = 0f;
        for (int i = 0; i < spline.GetPointCount(); i++)
        {
            float thisVal = spline.GetPosition(i).y;
            if (thisVal > mostVal)
            {
                idx = i;
                mostVal = thisVal; 
            }
        }
        return idx;
    }
}
