﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour
{
    GameObject snowball;

    IEnumerator fallCoroutine = null;
    private bool _fallen = false;
    public bool fallen
    {
        set
        {
            if (_fallen == false && value == true)
            {
                print("Starting fall coroutine");
                fallCoroutine = Fall();
                StartCoroutine(fallCoroutine);

            }
        }
    }

    void Start()
    {
        snowball = GameObject.FindGameObjectWithTag("Snowball");
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x + 10f < snowball.transform.position.x)
        {
            Destroy(gameObject);
        }
    }

    IEnumerator Fall()
    {
        var rb = GetComponent<Rigidbody2D>();
        var fall_speed = 5f;
        while (rb.rotation > -130f)
        {
            rb.rotation -= fall_speed * Time.deltaTime;
            fall_speed += 1f;
            yield return null;
        }
    }
}
