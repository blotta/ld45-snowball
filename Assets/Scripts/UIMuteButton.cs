﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMuteButton : MonoBehaviour
{
    [SerializeField] Sprite muteSpr;
    [SerializeField] Sprite unmuteSpr;

    [SerializeField] Image muteImage;

    private void Start()
    {
        if (GameObject.FindObjectOfType<SongGOSingle>().IsPlaying())
        {
            muteImage.sprite = muteSpr;
        }
        else
        {
            muteImage.sprite = unmuteSpr;
        }
    }

    public void Toggle()
    {
        if (muteImage.sprite == muteSpr)
        {
            Mute();
        }
        else if (muteImage.sprite == unmuteSpr)
        {
            Unmute();
        }
    }

    public void Mute()
    {
        muteImage.sprite = unmuteSpr;
        GameObject.FindObjectOfType<SongGOSingle>().StopSong();
    }

    public void Unmute()
    {
        muteImage.sprite = muteSpr;
        GameObject.FindObjectOfType<SongGOSingle>().RestartSong();
    }
}
