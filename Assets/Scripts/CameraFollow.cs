﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public float rate = 0.05f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 centerPoint = target.position + new Vector3(3f, -1.5f);

        Vector3 newPos = Vector3.Lerp(transform.position, centerPoint, Vector3.Distance(transform.position, target.position) / 20f);
        newPos.z = -10f;

        transform.position = newPos;
    }
}
