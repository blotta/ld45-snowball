﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skiier : MonoBehaviour
{
    [SerializeField] private float max_velocity;

    GameObject snowball;
    Rigidbody2D rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        snowball = GameObject.FindGameObjectWithTag("Snowball");
    }

    private void Update()
    {
        if (Vector3.Distance(snowball.transform.position, transform.position) > 100f ||
            transform.position.x + 10f < snowball.transform.position.x)

        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (rb.velocity.magnitude > max_velocity )
        {
            rb.velocity = rb.velocity.normalized * max_velocity;
        }
    }
}
