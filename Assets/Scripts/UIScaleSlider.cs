﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScaleSlider : MonoBehaviour
{
    GameObject snowball;
    Slider slider;

    float targetVal;
    float currentVal;

    [SerializeField] RectTransform treeImage;

    void Start()
    {
        currentVal = 0;
        snowball = GameObject.FindGameObjectWithTag("Snowball");
        slider = GetComponent<Slider>();
        var townAmount = snowball.GetComponent<Snowball>().townDropperScaleAmount;
        slider.maxValue = townAmount;
        //var treeAmount = snowball.GetComponent<Snowball>().treeDropperScaleAmount;
        //var treePercent = treeAmount / townAmount;

        ////var tree_rect = treeImage.rect;
        ////tree_rect.x = GetComponent<RectTransform>().rect.width * treePercent;
        ////treeImage.rect = tree_rect;
        //treeImage.sizeDelta = new Vector2(GetComponent<RectTransform>().rect.width * treePercent, treeImage.sizeDelta.y);
    }

    void Update()
    {
        targetVal = snowball.transform.localScale.x;

        currentVal = Mathf.Lerp(currentVal, targetVal, 0.05f);

        slider.value = currentVal;
    }
}
